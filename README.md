# Planets Explorer

Um projeto de exemplo para a disciplina INF1300 (PUC-RIO).

Baseado no exemplo desenvolvido por [Sergi Andre](https://sergiandreplace.com/planets-flutter-from-design-to-app/).

# Utilização

Basta efetuar um **git clone** neste repositório e abrir esta pasta em sua IDE de preferência. Recomendamos o uso do [Visual Studio Code](https://code.visualstudio.com/).