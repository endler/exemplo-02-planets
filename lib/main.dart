import 'package:flutter/material.dart';
import 'package:planets/homepage.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Planets",
      home: HomePage(),
    ),
  );
}
