import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:planets/gradientappbar.dart';
import 'package:planets/homepagebody.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          GradientAppBar("Planets"),
          HomePageBody(),
        ],
      ),
    );
  }
}
